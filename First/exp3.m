clc, clear all, close all;

f = @(x) log(x) - 1;
f1 = @(x) 1 / x;
x =  0.5:0.05:8;
y = f(x)
plot(x,y)
% oki3(f, f1, 10^(-15), 100, 5, 5)% график зависимости  погрешности от кол-ва итераций
% oki4(f,f1, 10.^(-15), 100, 5, 5)% График зависимости погрешности от заданной точности
% oki5(f,f1, 10.^(-15), 100, 5)% График зависимости использованного числа вычислений функции от заданной точности
% oki6(f,f1, 10.^(-15), 100, 5, 5)% График зависимости относительной погрешности решения от возмущения исходных данных
% oki7(f, 2)% Значение корня, полученное с помощью fzero и roots

function[] = oki3(f, f1, opacity, last, first, x0)
    xx = fzero(f,x0);
    [k, c, Kol, error] = Nuton(f, f1, opacity, last, first)
    
    for h = 1:length(error)
        error(h) = abs(error(h)-xx);
    end
    
    plot(Kol,error)
    grid minor
    figure
    semilogy(Kol,error)
    grid minor
end

function[] = oki4(f, f1, opacity, last, first, x0)
xx = fzero(f, x0);
ERROR = [];
Eps = [];
error = [];
a = opacity;
    
for r = 1:15
    [k, c, A, B] = Nuton(f, f1, a, last, first)
    error = [error k];
    Eps = [Eps a];
    a = a * 10;
end 
    
for I = 1:15
    length(error)
    G = abs(xx - error(I));
    ERROR = [ERROR G];
end

loglog(Eps, ERROR)
hold on
loglog(Eps, Eps)
grid minor
legend('Метод Ньютона', 'Биссектриса первого квадранта')
end

function [] = oki5(f, f1, opacity, last, first)
Kol = [];
for r = 1:1:15
    [k, c, A, error] = Nuton(f, f1, opacity, last, first)
    Kol = [Kol c];
    opacity = opacity * 10;
end 

EPS = [ -14 -13 -12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0];
plot(EPS,Kol)
grid minor
end

function [] = oki6(f, f1, opacity, last, first, x0)

xx = fzero(f, x0);
Kol = [0, 1, 2, 3, 4, 5];
R = [];

for j = 0:1:5
    if (rand < 0.5)
        d = -1;
    else
        d = 1;
    end
 
    f = @(x) log(x) - 1 * (1 + d * 0.01 * j * d + (rand * j * d * 0.001));
    
    [k, c, A, error] = Nuton(f, f1, opacity, last, first)
    R(j + 1) = k;
end
Rf = [];
for i = 1:6
    Rf(i) = abs(xx - R(i)) * 100 / xx;
end
plot(Kol, Rf)
grid minor
end

function[] = oki7(f,x0)

fzero(f,x0)

end
