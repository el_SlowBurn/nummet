clc, clear all, close all;

syms x1 x2 x3
c1 = sym2poly(x1 ^ 3 - 2 * x1 + 2);

% % для случая когда начальное приближение недостаточно близко к корню
% for i = 1:length(c2)
%     c2(i) = c2(i) / c2(1);
% end
% if max(c1) > 0 && min(c1) < 0  
%     f = @(x) x .^ 3 - 2 * x + 2;
%     f1 = @(x) 3 * x .^ 2 - 2;
%     [ansver, smt, smt1, smt2] = Nuton(f, f1, 10^(-15), 100, 0);
%     ansver
% end

% %   для случая когда поизводная в точке корня равна нулю
% f2 = @(x) x .^ 2;
% f3 = @(x) 2 * x;
% 
% [ansver, smt3, smt4, smt5] = Nuton(f2, f3, 10^(-15), 100, 0);
% ansver


% Если не существует вторая производная в точке корня, то скорость сходимости метода может быть заметно снижена
f4 = @(x) x + x .^ (4 / 3);
f5 = @(x) 1 + (4 / 3) * x .^(1 / 3);

f6 = @(x) x .^ 3 - 2 * x .^ 2  - 4 * x + 7;
f7 = @(x) 3 * x .^ 2 - 4 .* x - 4;
for i = 1:15
    opac(i) = 10 ^ (-i);
    [ansver1, smt6, smt7, roots1] = Nuton(f4, f5, opac(i), 100000, 1);
    [ansver2, smt8, smt9, roots2] = Nuton(f6, f7, opac(i), 100000, 1);
    iter1(i) = smt7(length(smt7));
    iter2(i) = smt9(length(smt9));
end
semilogy(iter1, opac, iter2, opac)
legend('вторая производная не существует', 'вторая производная существует')
ylabel('Точность')
xlabel('Количество иттераций')