clear all, clc, close all;
format long

%% Чтобы получить конкретный график или значение снимите комментарий слева от 
%% названия функции. Справа написано, что она делает
fun = @(x) x .* tan(x) - (1 / 3);

% bisection_1(0.2,fun)% График функции в окрестности корня(корней)  
 
% bisection_3(fun, 1, 2, 10.^(-15), 2)% график зависимости  погрешности от кол-ва итераций
% bisection_4(fun, 1, 2, 10.^(-15), 2)% График зависимости погрешности от заданной точности
% bisection_5(fun, 1, 2, 10.^(-15))% График зависимости использованного числа вычислений функции от заданной точности
bisection_6(fun, 1, 2, 10.^(-15), 2)% График зависимости относительной погрешности решения от возмущения исходных данных
% bisection_7(fun, 2)% Значение корня, полученное с помощью fzero и roots




function [] = bisection_1(a, fun)
   
    Xx = [];
    Yy = [];
    for i = 0.2:a:1
        Xx = [Xx i];
    end
    for j = 1:1:length(Xx)
        Yy(j) =  fun(Xx(j));
    end
    plot(Xx, Yy)
end



function [] = bisection_3(fun, a, b, eps, x0)
   
    xx = fzero(fun, x0);
    Rf = [];
    [c, j, Kol, R] = bisection_2(fun, a, b, eps, 100);
    for i = 1:j
        Rf(i) = abs(R(i) - xx);
    end
    
    plot(Kol, Rf)
    grid minor
    figure
    semilogy(Kol, Rf)
    grid minor
end


function [] = bisection_4(fun, aa, bb, eps, x0)
    
    xx = fzero(fun, x0);
    R = [];
    Rf = [];
    Kol = [];
    
    for j = 1:1:16
        [c, j, A, B] = bisection_2(fun, aa, bb, eps, 100);
        Kol = [Kol eps];
        eps = eps * 10;
        R = [R c];
    end
    
    for j = 1:1:16
        Rf(j) = abs(xx - R(j));
    end
   
    loglog(Kol, Rf)
    hold on
    loglog(Kol, Kol)
    grid minor
    legend('Метод половинного деления', 'Биссектриса первого квадранта')
end


function [Rf] = bisection_5(fun, aa, bb, eps)
   
    R = [];
    Rf = [];
    Kol = [];
    for j = -15:1:0
        Kol(j + 16) = j;
    end
    for j = 1:1:16
         [c, i, A, B] = bisection_2(fun, aa, bb, eps, 100);
        eps = eps * 10;
        R(j) = i;
    end
    plot(Kol, R)
grid minor
end

function [] = bisection_6(fun, aa, bb, eps, x0)
  xx=fzero(fun, x0);
    R = [];
    d = 0;
    Kol = [0, 1, 2, 3, 4, 5];
    for j = 0:1:5
        if (rand < 0.5)
            d = -1;
        else
            d = 1;
        end
        fun = @(x) x .^ 4 + 3 * x .^ 3 - 9 * x -9 * (1 + d * 0.01 * j * d + (rand * j * d * 0.001));
        [c, j, A, B]= bisection_2(fun, aa, bb, eps, 100);
        R = [R c];
    end
    Rf = [];
for i = 1:6
Rf(i) = abs((xx - R(i)) / xx) * 100;
end
plot(Kol, Rf)
grid minor
end


function[] = bisection_7(fun, x0)

fzero(fun, x0)
roots([1 3 0 -9 -9])
end
