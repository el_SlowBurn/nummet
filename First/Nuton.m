function [k, c, mas, mas2] = Nuton(func, func2, opacity, last, first)
    k = first;
    mas = []
    mas2 = []
    for i = 0:last
        a1 = func(k);
        b1 = func2(k);
        k = k - a1 / b1;
        mas = [mas i];
        mas2 = [mas2 k];
        if abs(a1) <= opacity
            break
        end
    end
    c = mas(length(mas))
end