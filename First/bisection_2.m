function [c,j,Kol,R] = bisection_3(fun,a,b,eps,n)
    
    R = [];
    Kol=[];
    
    Fa = fun(a);
    for j = 1:n
        c= (b + a)/2;
        y = fun(c);
        if y * Fa <0 
            b = c;
        else
            a = c;
            Fa = y;
        end
        
        R(j) = c;
        Kol = [Kol j];
        if abs(b - a) < eps
            break
        end
    end
    
end