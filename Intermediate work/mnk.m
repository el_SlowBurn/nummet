clc, clear all, close all;
f=@(x) 3*x.^4 -8*x.^3 -18*x.^2 +6;
xx=linspace(-10,10,50);
yy=f(xx)+randn(1,50);


P1=polyfit(xx,yy,1);
P11=polyval(P1,xx);

P2=polyfit(xx,yy,2);
P22=polyval(P2,xx);

P3=polyfit(xx,yy,3);
P33=polyval(P3,xx);

P4=polyfit(xx,yy,4);
P44=polyval(P4,xx);

P5=polyfit(xx,yy,5);
P55=polyval(P5,xx);

Er=yy-P44;

figure
subplot(6,1,1)
plot(xx,yy,'.')
hold on
plot(xx,P11)
subplot(6,1,2)
plot(xx,yy,'.')
hold on
plot(xx,P22)
subplot(6,1,3)
plot(xx,yy,'.')
hold on
plot(xx,P33)
subplot(6,1,4)
plot(xx,yy,'.')
hold on
plot(xx,P44)
subplot(6,1,5)
plot(xx,yy,'.')
hold on
plot(xx,P55)
subplot(6,1,6)
plot(xx,Er)

cftool
Y = yy;
Y(1:10:10) = yy(1:10:10) + 100;


x1 = linspace(-10, 10, 100);
f1=@(x) x - sin(x);
y1 =f1(x1)+randn(size(x1));
