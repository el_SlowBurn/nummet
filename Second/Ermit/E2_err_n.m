clc, clear all, close all
%ошибка интерполяции от степени полинома
%функция 2 

% f = @(x) x .* tan(x) - (1 / 3);
f = @(x) x .^ 3 - 2 * x .^ 2  - 4 * x + 7;
x = -2:0.02:2;
b = 100;
c = linspace(-2, 2, b); %составляем множество точек

for n = 5:250
    ee = [];
    X = linspace(-2, 2, n); %узлы, границы и кол-во точек (кол-во отрезков, на 1 меньше)
    Y = X .^ 3 - 2 * X .^ 2  - 4 * X + 7;
%     Y = X .* tan(X) - (1 / 3); %функция
    TT = Ermit(X, Y);
    for k = 1:b
        for j = 1: length(X)-1
            if c(k) <= X(j + 1) && c(k) >= X(j)
                E = TT(j, 1) .* (c(k) - X(j)) .^ 3 + TT(j, 2) .* (c(k) - X(j)) .^ 2 + TT(j, 3) .* (c(k) - X(j)) + TT(j, 4);
            end
        
        end

        %E = TT(j,1).*(c-X(j)).^3 + TT(j,2).*(c-X(j)).^2 + TT(j,3).*(c-X(j)) + TT(j,4);
        %E = TT(j,1).*(x1-X(j)).^3 + TT(j,2).*(x1-X(j)).^2 + TT(j,3).*(x1-X(j)) + TT(j,4);
        e = abs(E - f(c(k)));
        ee = [ee e];

    end

    d(n) = max(ee);

end

semilogy(1:250, d, 'b-')
title("Зависимость поточечной ошибки от степени полинома")
xlabel("Степень полинома n")
ylabel("Ошибка уклонения (пролагрифмированная)")
hold on
grid on



