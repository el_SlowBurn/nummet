%зависимость ошибки интерполяционного полинома Эрмита при возмущении данных
clc, close all, clear all
% f = @(x) x .* tan(x) - (1 / 3);
f = @(x) x .^ 3 - 2 * x .^ 2  - 4 * x + 7;
RR = [];
RMM = [];

for b = 1:5
    n = 10;
    X = linspace(-2, 2, n);
    Y = (f(X)) .* (1 + rand * b / 100);
    TT = Ermit(X, Y);
    RR = [];
    for j = 1 : (length(X) - 1)
        xx = linspace(X(j), X(j + 1), n);
        E = TT(j, 1) .* (xx - X(j)) .^ 3 + TT(j, 2) .* (xx - X(j)) .^ 2 + TT(j, 3) .* (xx - X(j)) + TT(j, 4);
        R = max(abs((E - f(xx)) / abs(f(xx))));
        RR = [RR R];
    end

    RM = max(RR);
    RMM = [RMM RM];
end
plot(1:5, RMM, "o-")
title("Зависимость относительной ошибки функции от возмущения")
xlabel("Процент возмущения")
ylabel("Относительная ошибка")
grid on


