clc, close all, clear all
%2 dots

x1 = 0.74;
% f = @(x) x .* tan(x) - (1 / 3); 
f = @(x) x .^ 3 - 2 * x .^ 2  - 4 * x + 7;
N = 250;
ee = [];
figure
for n = 5:N
    X = linspace(-2, 2, n);
%     Y = X .* tan(X) - (1 / 3);
    Y = X .^ 3 - 2 * X .^ 2  - 4 * X + 7;
    TT = Ermit(X, Y);
    for j = 1 : length(X)-1
        if x1 <= X(j + 1) && x1 >= X(j)
            E = TT(j, 1) .* (x1 - X(j)) .^ 3 + TT(j, 2) .* (x1 - X(j)) .^ 2 + TT(j, 3) .* (x1 - X(j)) + TT(j, 4);
        end
   end
   e = abs(E - f(x1));
   ee = [ee e];
end
semilogy(5:N, ee)
title("Зависимость ошибки в первой точке от количества узлов")
hold on
grid on
figure


x2 = -0.15;
% f = @(x) x .* tan(x) - (1 / 3);
f = @(x) x .^ 3 - 2 * x .^ 2  - 4 * x + 7;
N = 250;
ee = [];
for n = 5:N
    X = linspace(-2, 2, n);
%     Y = X .* tan(X) - (1 / 3);
    Y = X .^ 3 - 2 * X .^ 2  - 4 * X + 7;
    TT = Ermit(X, Y);
    for j = 1 : length(X) - 1
        if x2 <= X(j + 1) && x2 >= X(j)
            E = TT(j, 1) .* (x2 - X(j)) .^ 3 + TT(j, 2) .* (x2 - X(j)) .^ 2 + TT(j, 3) .* (x2 - X(j)) + TT(j, 4);
        end
    end
   e = abs(E - f(x2));
   ee = [ee e];
end
semilogy(5:N, ee)
title("Зависимость ошибки во второй точке от количества узлов")
hold on
grid on