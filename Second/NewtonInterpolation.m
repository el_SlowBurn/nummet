clc, clear all

f = @(x) x .* tan(x) - (1 / 3);
% f = @(x) x .^ 3 - 2 * x .^ 2  - 4 * x + 7;
% f = @(x) x .^ 2 + 2;

% inter1L(f,5) %(Графики :  функции, 3-х интерполяционных полиномов для
             %нее,ошибка)Равномерная сетка
% inter1CHE(f,5) %Графики :  функции, 3-х интерполяционных полиномов для
             %нее,ошибка)Чебышевская сетка
% inter2L(f)% (График ошибки от количества узлов)Равномерная сетка
% inter2CHE(f)% (График ошибки от количества узлов)Чебышевская сетка
% inter3L(f,-1,5)%(График ошибки от количества узлов в 2 точках)Равномерная сетка
% inter3CHE(f,-1,5)%(График ошибки от количества узлов в 2 точках)Чебышевская сетка
% inter4L(f,10)%(График относительной погрешности от процента возмущений исходных данных)
            %Равномерная сетка
% inter4CHE(f,14)%(График относительной погрешности от процента возмущений исходных данных)
            %Чебышевская сетка

            
            
function [] = inter1L(f,a)
    x = -2 : 0.1 : 2;
    y = f(x);

    X_int = -10 : 20 / (a - 1) : 10;
    Y_int = f(X_int);
    for i = 1 : length(x)
        Poly1(i) = Nuton_int(X_int, Y_int, x(i));
    end

    X_int = -10 : 20 / (1.5 * (a - 1)) : 10;
    Y_int = f(X_int);
    for i = 1 : length(x)
        Poly2(i) = Nuton_int(X_int, Y_int, x(i));
    end

    
    X_int = -10 : 20/(2*(a-1)) : 10;
    Y_int = f(X_int);
    for i = 1 : length(x)
        Poly3(i) = Nuton_int(X_int, Y_int, x(i));
    end
    
    plot(x,y)
    hold on
    plot(x,Poly1)
    hold on
    plot(x,Poly2)
    hold on
    plot(x,Poly3)
    grid minor
    xlabel('x');
    ylabel('y')
    title('Иллюстрация работы полиномов');
    legend('функция', 'полином 5 степени', 'полином 8 степени', 'полином 10 степени');
    
    P0=[];
    for i=1 : length(x)
        P0=[P0 NaN];
        Poly1(i)=abs(Poly1(i)-y(i));
        Poly2(i)=abs(Poly2(i)-y(i));
        Poly3(i)=abs(Poly3(i)-y(i));
    end
    figure
    plot(x,P0)
    hold on
    plot(x,Poly1)
    hold on
    plot(x,Poly2)
    hold on
    plot(x,Poly3)
    hold on
    grid minor
    xlabel('x');
    ylabel('y')
    title('График ошибки в каждой точке');
    legend('', 'полином 5 степени', 'полином 8 степени', 'полином 10 степени');
end

function [] = inter1CHE(f,a)
    X_int=[];
    Y_int=[];

    for i=1 : a
        X_int=[X_int (0.5*(-1)*20*cos((2*i -1)*3.14/(2*a)))];
    end
    for i=1 : length(X_int)
        Y_int=[Y_int f(X_int(i))];
    end
    x = -2 : 0.1 : 2;
    y = [];
    for i = 1 : length(x)
        y = [y f(x(i))];
    end
    for i = 1 : length(x)
        Poly1(i) = Nuton_int(X_int, Y_int, x(i));
    end
    X_int=[];
    Y_int=[];

    for i=1 : 1.5*a
        X_int=[X_int (0.5*(-1)*20*cos((2*i -1)*3.14/(3*a)))];
    end
    for i=1 : length(X_int)
        Y_int=[Y_int f(X_int(i))];
    end
    for i = 1 : length(x)
        Poly2(i) = Nuton_int(X_int, Y_int, x(i));
    end
    
    X_int = [];
    Y_int = [];
    for i = 1 : 2*a
        X_int = [X_int (0.5*(-1)*20*cos((2*i -1)*3.14/(4*a)))];
    end
    for i = 1 : length(X_int)
        Y_int = [Y_int f(X_int(i))];
    end
     for i = 1 : length(x)
        Poly3(i) = Nuton_int(X_int, Y_int, x(i));
    end
    
    plot(x,y)
    hold on
    plot(x,Poly1)
    hold on
    plot(x,Poly2)
    hold on
    plot(x,Poly3)
    grid minor
    xlabel('x');
    ylabel('y')
    title('Иллюстрация работы полиномов');
    legend('функция','полином 5 степени','полином 8 степени','полином 10 степени');
    
    P0=[];
    for i=1 : length(x)
        P0=[P0 NaN];
        Poly1(i)=abs(Poly1(i)-y(i));
        Poly2(i)=abs(Poly2(i)-y(i));
        Poly3(i)=abs(Poly3(i)-y(i));
    end
    figure
    plot(x,P0)
    hold on
    plot(x,Poly1)
    hold on
    plot(x,Poly2)
    hold on
    plot(x,Poly3)
    hold on
    grid minor
    xlabel('x');
    ylabel('y')
    title('График ошибки в каждой точке');
    legend('','полином 5 степени','полином 8 степени','полином 10 степени');
end

function [] = inter2L(f)
    SS=[];
    for a = 10 : 100
        X_int = -2 : 20/(a-1) : 2;
        Y_int=[];
        for i=1 : length(X_int)
            Y_int=[Y_int f(X_int(i))];
        end
        
        x = -2 : 0.1 : 2;
        y=[];
        for i=1 : length(x)
            y=[y f(x(i))];
        end
        for i = 1 : length(x)
            Poly(i) = Nuton_int(X_int, Y_int, x(i));
        end
        for i=1 : length(x)
            Poly(i)=abs(Poly(i)-y(i));
        end
        S= max(abs(Poly));
        SS=[SS S];
    end
    A=[];
    for a=10 : 100
        A=[A a];
    end
    semilogy(A,SS)
    grid on
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов');
   
end

function [] = inter2CHE(f)
    SS = [];
    for a = 10 : 100
        X_int = [];
        Y_int = [];

        for i = 1 : a
            X_int = [X_int (0.5*(-1)*20*cos((2*i -1)*3.14/(2*a)))];
        end
        for i = 1 : length(X_int)
            Y_int = [Y_int f(X_int(i))];
        end
        x = -2 : 0.1 : 2;
        y = [];
        for i = 1 : length(x)
            y = [y f(x(i))];
        end
        for i = 1 : length(x)
            Poly(i) = Nuton_int(X_int, Y_int, x(i));
        end
  
        for i = 1 : length(x)
            Poly(i) = abs(Poly(i) - y(i));
        end
        S = max(abs(Poly));
        SS = [SS S];
    end
    A = [];
    for a = 10 : 100
        A = [A a];
    end
    semilogy(A, SS)
    grid on
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов');
end

function [] = inter3L(f,x1,x2)
    PP1 = [];
    PP2 = [];
    A = [];
     for a = 10 : 100
        X_int = -2 : 20 / (a - 1) : 2;
        Y_int = [];
        for i = 1 : length(X_int)
            Y_int = [Y_int f(X_int(i))];
        end
%         Y1 = Nuton_int(X_int, Y_int);
        A1 = f(x1);
        A2 = f(x2);
        PP1 = [PP1 abs(Nuton_int(X_int, Y_int, x1) - A1)];
        PP2 = [PP2 abs(Nuton_int(X_int, Y_int, x2) - A2)];
        A = [A a];
     end
    semilogy(A,PP1)
    hold on
    semilogy(A,PP2)
    grid minor
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов в 2 точках');
end

function [] = inter3CHE(f,x1,x2)
    PP1 = [];
    PP2 = [];
    A = [];
     for a = 10 : 100
        X_int = [];
        Y_int = [];

        for i = 1 : a
            X_int = [X_int (0.5*(-1)*20*cos((2*i -1)*3.14/(2*a)))];
        end
        for i=1 : length(X_int)
            Y_int = [Y_int f(X_int(i))];
        end
        A1 = f(x1);
        A2 = f(x2);
        PP1 = [PP1 abs(Nuton_int(X_int, Y_int, x1) - A1)];
        PP2 = [PP2 abs(Nuton_int(X_int, Y_int, x2) - A2)];
        A = [A a];
     end
    semilogy(A,PP1)
    hold on
    semilogy(A,PP2)
    grid minor
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов в 2 точках');
end

function[] = inter4L(f,a)
    SS = [];
    A = [];
    for j = 0 : 5
        X_int = [];
        Y_int = [];
        for i = -2 : 20 / (a - 1) : 2
            if (rand < 0.5)
                d = -1;
            else
                d = 1;
            end
            X_int = [X_int i * (1 + d * 0.01 * j * d + (rand * j * d * 0.001))];
        end
        for i = 1 : length(X_int)
            Y_int = [Y_int f(X_int(i))];
        end
        x = -2 : 0.1 : 2;
        y = [];
        for i = 1 : length(x)
            y = [y f(x(i))];
        end
        for i = 1 : length(x)
            Poly(i) = Nuton_int(X_int, Y_int, x(i));
        end
        PP = [];
        for i = 1 : length(x)
            if y(i) ~= 0
                PP(i) = abs((Poly(i) - y(i)) / y(i));
            end
        end
        
        S = max(PP);
        SS = [SS S];
        A = [A j];
    end
   
    plot(A,SS)
    grid on
    xlabel('процент возмущений исходных данных');
    ylabel('относительная погрешность')
    title('Зависимость относительной погрешности от процента возмущений исходных данных');
end

function[]= inter4CHE(f,a)
    SS = [];
    A = [];
    for j = 0 : 5
        X_int = [];
        Y_int = [];
        for i = 1 : a
            if (rand < 0.5)
                d = -1;
            else
                d = 1;
            end
            X_int = [X_int (0.5 * (-1) * 20 * cos((2 * i - 1) * 3.14 / (2 * a))) * (1 + d * 0.01 * j * d + (rand * j * d * 0.001))];
        end
        for i = 1 : length(X_int)
            Y_int = [Y_int f(X_int(i))];
        end
        x = -2 : 0.1 : 2;
        y = [];
        for i = 1 : length(x)
            y = [y f(x(i))];
        end
        for i = 1 : length(x)
            Poly(i) = Nuton_int(X_int, Y_int, x(i));
        end
        PP = [];
        for i = 1 : length(x)
            if y(i) ~= 0
                PP(i) = abs((Poly(i) - y(i)) / y(i));
            end
        end
        
        S = max(PP);
        SS = [SS S];
        A = [A j];
    end
    plot(A, SS)
    grid on
    xlabel('процент возмущений исходных данных');
    ylabel('относительная погрешность')
    title('Зависимость относительной погрешности от процента возмущений исходных данных');
end