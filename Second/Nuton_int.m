function [Y] = Nuton_int(x, y, Xn)
    h = x(2) - x(1);
    q = (Xn - x(1)) / h;
    Dy(:, 1) = y'; 
    for i = 2:length(x)
        for j = 1:length(x) - i + 1 
            Dy(j, i) = Dy(j + 1, i - 1) - Dy(j, i - 1);
        end
    end
    Y = Dy(1, 1);
    for i = 2:length(x)
        Q = 1;
        for j = 1:i - 1
            Q = Q * (q - j + 1) ;
        end
        fact = 1;
        for k = 1 : i - 1
            fact = fact * k;
        end 
        Y = Y + Dy(1, i) * Q / fact;
    end
end
       