function [l, u] = LUdecompose(A)
    [row, col] = size(A);
    l = eye(row, col)
    u = zeros(row, col)
    for i = 1:row
        for j = 1:col
            if i <= j
                sum = 0;
                for k = 1:i
                    sum = sum + l(i, k) * u(k, j);
                end
                u(i, j) = A(i, j) - sum;
            else
                sum = 0;
                for k = 1:j
                    sum = sum + l(i, k) * u(k, j);
                end
                l(i, j) = (A(i, j) - sum) / u (j, j);
            end
        end
    end
end
    