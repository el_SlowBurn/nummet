function Y = ExplEuler(f, t0, h, tf, y, Y)
    for t = t0:h:tf    
        y = y + h*f(t,y);
        Y = [Y y];
    end
end