%ЗАДАНИЕ 2
x0=1;
y0=1/(2*log(2));
f=@(x, y) (-y./(x+1)-y.^2);
y1=@(x) 1./((x+1).*log(x+1));


Err = [];
Iter=[];
Tol = [];
tol = 1e-1;
for i = 1 : 9
     err=0;
    tspan = [1 5];
    opts = odeset('RelTol',tol, 'AbsTol',tol, 'Stats','on');
    [t,y] = ode23(f, tspan, y0, opts);
    iter=length(t);
%     plot(t,y, 'Linewidth', 1.5)
    
    %y1 = sqrt(1./(2.*t.^4.*exp(t)))
    for j=1:length(t)
        err=err+abs(y(j)-y1(t(j)));
    end
    err=err/length(t);
    Tol = [Tol tol];
    Iter=[Iter iter]
    Err = [Err err];
    tol = tol / 10
end
plot(t,y, 'Linewidth', 1.5)
xlabel('t')
ylabel('y')
grid on
grid minor
hold on 
plot(t,y1(t),'Linewidth', 1.5)
title('численное и точное решения ode 23')
legend('численное решение', 'точное решение')
hold off 

loglog(Tol, Err, 'Linewidth', 1.5)
xlabel('Точность')
ylabel('Погрешность')
title('Зависимость погрешности от точности для ode23')
grid on
grid minor

plot(log(Tol), Iter, 'Linewidth', 1.5)
xlabel('Точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности для ode23')
grid on
grid minor

% ode45
Err = [];
Iter=[];
Tol = [];
tol = 1e-1;
for i = 1 : 9
    err=0;
    tspan = [1 5];
    opts = odeset('RelTol',tol, 'AbsTol',tol, 'Stats','on');
    [t, y] = ode45(f, tspan, y0, opts);
    iter=length(t);
    for j=1:length(t)
        err=err+abs(y(j)-y1(t(j)));
    end
    err=err/length(t);
    Tol = [Tol tol];
    Iter=[Iter iter];
    Err = [Err err];
    tol = tol / 10
end
plot(t, y, 'Linewidth',1.5)
xlabel('t')
ylabel('y')
grid on
grid minor
hold on 
plot(t,y1(t),'Linewidth',1.5)
title('численное и точное решения ode 45')
legend('численное решение', 'точное решение')

hold off 
loglog(Tol, Err, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Погрешность')
title('Зависимость погрешности от точности для ode45')
grid on
grid minor

semilogx(Tol, Iter, 'Linewidth', 1.5)
xlabel('Точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности для ode45')
grid on
grid minor

% ode23tb
Err = [];
Iter=[];
Tol = [];
tol = 1e-1;
for i = 1 : 9
    err=0;
    tspan = [1 5];
    opts = odeset('RelTol',tol, 'AbsTol',tol, 'Stats','on');
    [t,y] = ode23tb(f, tspan, y0, opts);
    iter=length(t);
    for j=1:length(t)
        err=err+abs(y(j)-y1(t(j)));
    end
    err=err/length(t);
    Tol = [Tol tol];
    Iter=[Iter iter];
    Err = [Err err];
    tol = tol / 10
end
plot(t, y, 'Linewidth',1.5)
xlabel('t')
ylabel('y')
grid on
grid minor
hold on 
plot(t,y1(t),'Linewidth',1.5)
title('численное и точное решения ode 23tb')
legend('численное решение', 'точное решение')
hold off 

loglog(Tol, Err, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Погрешность')
title('Зависимость погрешности от точности для ode23tb')
grid on
grid minor

semilogx(Tol, Iter, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности для ode23tb')
grid on
grid minor

% ode113
Err = [];
Iter=[];
Tol = [];
tol = 1e-1;
for i = 1 : 9
    err=0;
    tspan = [1 5];
    opts = odeset('RelTol',tol, 'AbsTol',tol, 'Stats','on');
    [t,y] = ode113(f, tspan, y0, opts);
    iter=length(t);
    for j=1:length(t)
        err=err+abs(y(j)-y1(t(j)));
    end
    err=err/length(t);
    Tol = [Tol tol];
    Iter=[Iter iter];
    Err = [Err err];
    tol = tol / 10
end
plot(t, y, 'Linewidth',1.5)
xlabel('t')
ylabel('y')
grid on
grid minor
hold on 
plot(t,y1(t),'Linewidth',1.5)
title('численное и точное решения ode 113')
legend('численное решение', 'точное решение')
hold off 

loglog(Tol, Err, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Погрешность')
title('Зависимость погрешности от точности для ode113')
grid on
grid minor

semilogx(Tol, Iter, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности для ode113')
grid on
grid minor

% ode78
Err = [];
Iter=[];
Tol = [];
tol = 1e-1;
for i = 1 : 9
    err=0;
    tspan = [1 5];
    opts = odeset('RelTol',tol, 'AbsTol',tol, 'Stats','on');
    [t,y] = ode78(f, tspan, y0, opts);
    iter=length(t);
    for j=1:length(t)
        err=err+abs(y(j)-y1(t(j)));
    end
    err=err/length(t);
    Tol = [Tol tol];
    Iter=[Iter iter];
    Err = [Err err];
    tol = tol / 10
end
plot(t, y, 'Linewidth',1.5)
xlabel('t')
ylabel('y')
grid on
grid minor
hold on 
plot(t,y1(t),'Linewidth',1.5)
title('численное и точное решения ode 78')
legend('численное решение', 'точное решение')
hold off 

loglog(Tol, Err, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Погрешность')
title('Зависимость погрешности от точности для ode78')
grid on
grid minor

semilogx(Tol, Iter,'Linewidth',1.5)
xlabel('Точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности для ode78')
grid on
grid minor

% ode89
Err = [];
Iter=[];
Tol = [];
tol = 1e-1;
for i = 1 : 9
    err=0;
    tspan = [1 5];
    opts = odeset('RelTol',tol, 'AbsTol',tol, 'Stats','on');
    [t,y] = ode89(f, tspan, y0, opts);
       iter=length(t);
    for j=1:length(t)
        err=err+abs(y(j)-y1(t(j)));
    end
    err=err/length(t);
    Tol = [Tol tol];
    Iter=[Iter iter];
    Err = [Err err];
    tol = tol / 10
end

plot(t, y, 'Linewidth',1.5)
xlabel('t')
ylabel('y')
grid on
grid minor
hold on 
plot(t,y1(t),'Linewidth',1.5)
title('численное и точное решения ode 89')
legend('численное решение', 'точное решение')
hold off 

loglog(Tol, Err, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Погрешность')
title('Зависимость погрешности от точности для ode89')
grid on
grid minor

semilogx(Tol, Iter, 'Linewidth',1.5)
xlabel('Точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности для ode89')
grid on
grid minor

%ЗАДАНИЕ 3
x0=1;
y0=0;
f=@(x, y) (1/x)*((2*y/log(x))+1);
y1=@(x) (log(x)).^2-log(x);
t0 = 1;
tf = 5;
h = 0.0001;
Y = [];
T = t0:h:tf;
Y = ExplEuler(f, t0, h, tf, y0, Y);
plot(T,Y,'Linewidth',1.5)
hold on
plot(T,y1(T),'Linewidth',1.5)
title('Решение ДУ явным методом Эйлера')
legend('Решение явным методом Эйлера','Точное решение')
xlabel('Ось t')
ylabel('Ось y')
grid on
grid minor

% ЗАДАНИЕ 4
%4.1
%шаг 0.01
f = @(t, y) 5*(y - t^2);
t0 = 0.08;
tf = 0.01;
h = -0.01;
Y = [];
y = 0.08;
T = t0:h:tf;
Y = ExplEuler(f, t0, h, tf, y, Y);
plot(T,Y,'Linewidth',1.5)
hold on
y1 =@(t) t.^ + 0.4.*t + 0.08;
plot(T,y1(T),'Linewidth',1.5)
title('Решение ДУ явным методом Эйлера, h = -0.01')
legend('Решение явным методом Эйлера','Точное решение')
xlabel('Ось t')
ylabel('Ось y')
grid on
grid minor

%переменный шаг
f = @(t, y) 5*(y - t^2);
t0 = 0.08;
tf = 0.01;
h = -0.01;
Y = [];
y = 0.08;
T=[];
j = -0.01;
k = t0;
while k > tf
    T = [T t0];
    t0 = t0 + j;
    j = j - 0.005;
    k = t0;
end
for t = T   
    y = y + h*f(t,y);
    Y = [Y y];
end
plot(T,Y,'Linewidth',1.5)
hold on
y1 =@(t) t.^ + 0.4.*t + 0.08;
plot(T,y1(T),'Linewidth',1.5)
title('Решение ДУ явным методом Эйлера, переменный шаг')
legend('Решение явным методом Эйлера','Точное решение')
xlabel('Ось t')
ylabel('Ось y')
grid on
grid minor

%4.2
f = @(t, y) 5*(y - t^2);
t0 = 0.08;
tf = 0.5;
h = 0.1;
Y = [];
y = 0.08;
T = t0:h:tf;
Y = ExplEuler(f, t0, h, tf, y, Y);
y1 =@(t) t.^ + 0.4.*t + 0.08;
plot(T,y1(T),'Linewidth',1.5)
hold on
plot(T,Y,'Linewidth',1.5)
h = 0.01;
Y = [];
T = t0:h:tf;
Y = ExplEuler(f, t0, h, tf, y, Y);
plot(T,Y,'Linewidth',1.5)
h = 0.001;
Y = [];
T = t0:h:tf;
Y = ExplEuler(f, t0, h, tf, y, Y);
plot(T,Y,'Linewidth',1.5)
title('Решение ДУ явным методом Эйлера, [0.08:h:0.5]')
legend('Точное решение', 'h=0.1', 'h=0.01', 'h=0.001')
xlabel('Ось t')
ylabel('Ось y')
grid on
grid minor

%4.3 (тут несколько раз поменять t0 и tf)
f = @(t, y) 5*(y - t^2);
t0 = 0.08;
tf = 0.5;
h = 0.01;
err=0
Err = [];
H = [];
for i = 1 : 8
    Y = [];
    y = 0.08;
    T = t0:h:tf;
    Y = ExplEuler(f, t0, h, tf, y, Y);
    y1 = @(t) t.^2 + 0.4.*t + 0.08;
    for j=1:length(T)
        err=err+abs(Y(j)-y1(T(j)));
    end
    err=err/length(T);
    Err = [Err err];
    H = [H h];
    h = h / 5;
    i
end
semilogx(H, Err, 'LineWidth',1.5)
title('Зависимость погрешности от размера шага [0; 1]')
xlabel('Размер шага')
ylabel('Погрешность')
grid on
grid minor

% ЗАДАНИЕ 5
x0=1;
y0=0;
f=@(x, y) (1/x)*((2*y/log(x))+1);
y1=@(x) (log(x)).^2-log(x);
t0 = 1;
tf = 5;
h = 0.0001;
N = (tf - t0) / h;
t = linspace(t0, tf, N+1);
y = zeros(size(t));
y(1) = y0;
y = implEuler(f, N, y, t, h);
plot(t,y,'Linewidth',1.5)
hold on
plot(t,y1(t),'Linewidth',1.5)
title('Решение ДУ неявным методом Эйлера')
legend('Решение неявным методом Эйлера','Точное решение')
xlabel('Ось t')
ylabel('Ось y')
grid on
grid minor

% ЗАДАНИЕ 6
%6.1
h = 0.001;
Err = [];
Err1=[];
H = [];
f = @(t, y) -100.*y + 10;
y1 = @(t) 1/10 + 9/10.*exp(-100.*t);
t0 = 0.08;
tf = 1;
for j = 1 : 6
    N = (tf - t0) / h;
    y0 = 1;
    t = linspace(t0, tf, N+1);
    y = zeros(size(t));
    y(1) = y0;
    y = implEuler(f, N, y, t, h);
    for i=1:length(t)
        err=err+abs(y(i)-y1(t(i)));
    end
    err=err/length(t);
    H = [H h];
    Err = [Err err];
    T = t0:h:tf;
    h = h / 5;
    j
end

plot(t,y1(t),'Linewidth',1.5)
hold on
plot(t,y,'Linewidth',1.5)
title('Решение ДУ неявным методом Эйлера, h = 0.001')
legend('Точное решение', 'Решение неявным методом Эйлера')
xlabel('Ось t')
ylabel('Ось y')
grid on
grid minor

semilogx(H, Err, 'LineWidth', 1.5)
grid on
grid minor
title('Зависимость погрешности от шага')
xlabel('Шаг')
ylabel('Погрешность')

%жесткие системы
S=[10^2 2.5*10^2 5*10^2 7.5*10^2 10^3 2*10^3 4*10^3 6*10^3 8*10^3 10^4 2*10^4 4*10^4 6*10^4 8*10^4 10^5 2*10^5 4*10^5 6*10^5 8*10^5 10^6]
y0=[1; 0; 1; 0; 1; 0; 1; 0; 1; 0];
a=0;
SS=[];
ERR1=zeros(10, 20);
ERR2=zeros(10, 20);

for i = 1:20
    SS=[SS S(i)];
    L=linspace(-1, -S(i), 10);
    A=diag(L);
    A1=rand(10);
    [Q, R]=qr(A1);
    A=Q'*A*Q;
    f=@(x, y)A*y;    
    opts=odeset('RelTol', 1e-10,'AbsTol', 1e-15);
    Err1=zeros(10, 1);
    Err2=zeros(10, 1);    
    k=1;    
    for j = 0.1:-0.01:0.01
        b=j*100;
        N=100;
        [~, Y0]=ode15s(f, [a b], y0, opts);
        Y1=[];
        Y2=zeros(10, N+1);
        Y2(:, 1)=y0;
        Y1=ExplEuler(f, a, j, b, y0, Y1);
        Y2=ImplEulerVec(A, N, Y2, j);
        Err1(k)=norm(Y0(end, :)-Y1(:, end));
        Err2(k)=norm(Y0(end, :)-Y2(:, end));
        k=k+1;
    end
    ERR1(:, i)=Err1;
    ERR2(:, i)=Err2;
end

loglog(SS, ERR1(1, :))
hold on
for i = 2 : 10
    loglog(SS, ERR1(i, :))
end
grid on
xlabel('Жесткость')
ylabel('Фактическая погрешность')
title('Зависимость фактической погрешности от жесткости для разных шагов, явный метод Эйлера')
legend('j=0.1', 'j=0.09', 'j=0.08', 'j=0.07', 'j=0.06', 'j=0.05', 'j=0.04', 'j=0.03', 'j=0.02', 'j=0.01')


loglog(SS, ERR2(1, :))
hold on
for i = 2 : 10
    loglog(SS, ERR2(i, :))
end
grid on
xlabel('Жесткость')
ylabel('Фактическая погрешность')
title('Зависимость фактической погрешности от жесткости для разных шагов, неявный метод Эйлера')
legend('j=0.1', 'j=0.09', 'j=0.08', 'j=0.07', 'j=0.06', 'j=0.05', 'j=0.04', 'j=0.03', 'j=0.02', 'j=0.01')
