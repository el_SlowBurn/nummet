function y = ImplEulerVec(A, n, y, h)
N=size(A, 1);
for i=2:n+1
    M=eye(N)-h*A;
    y(:, i)=M\y(:, i-1);
end
end