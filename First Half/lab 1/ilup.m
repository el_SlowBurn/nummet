function [A, lev] = ilup(A, p)
[n,n] = size(A);
lev=zeros(size(A));
for i=1:n
    for j = 1:n
        if (A(i,j)~= 0) || (i==j)
            lev(i,j)=0;
        else
            lev(i,j)=Inf;
        end
    end
end
for i=2:n
    for k=1:i-1
        if lev(i,k) <= p
            A(i,k)=A(i,k)/A(k,k);
            for j = k+1:n
                A(i,j)=A(i,j)-A(i,k)*A(k,j);
                if A(i,j) ~= 0
                    lev(i,j)=min(lev(i,j),lev(i,k)+lev(k,j)+1);
                end
            end
        end
    end
    for j = 1:n
        if lev(i,j) > p
            A(i,j)=0; 
        end
    end
end