T = [];
S = [];
e = 10^(-15);
maxN = 5000;
for i = 5:2:53
    G=numgrid('H', i);
    A=delsq(G); 
    n=size(A, 2);
    x0=ones(n, 1);
    b=A*x0;
    xt=zeros(n,1);
    t=0;
    tic
    P =ilup(A,2);
    [x, k, m]=conjgrad(A, xt, P, b,  e, maxN);
    t = toc
    T=[T t];
    S=[S n];
end
plot(S, T)
xlabel('Размер');
ylabel('Время');
title('Зависимость времени выполнения от размера матрицы');
grid minor

clc, clear, close all
e = 10^(-15);
maxN = 5000;
S = [];
E = [];
for i = 5:2:53
    G=numgrid('H', i);
    A=delsq(G); 
    P =ilup(A,2);
    n=size(A, 2);
    x0=ones(n, 1);
    xt=zeros(n,1);
    b=A*x0;
    [x, k, m]=conjgrad(A, xt, P, b,  e, maxN);
    err=norm(x-x0)
    E = [E err]
    S = [S n];
end

semilogy(S, E)
xlabel('Размер');
ylabel('Точность');
title('Зависимость точности от размера матрицы');
grid minor

clc, clear, close all
maxN = 5000;
E = [];
D = [];
for i = 1:15
    G=numgrid('H', 25);
    A=delsq(G); 
    P =ilup(A,1);
    n=size(A, 2);
    x0=ones(n, 1);
    xt=zeros(n,1);
    b=A*x0;
    e = 10^(-i);
    E = [E e];
    [x k, m] =conjgrad(A, xt, P, b,  e, maxN);
    err=norm(x-x0)/norm(x0)
    D = [D err];
end

figure
x = 1e-15:1e-4:1e-1;
y = @(x) x;
loglog(E, D)
hold on
plot(x, y(x))
ylim([10^(-15) 1])
xlabel('Точность');
ylabel('Погрешность');
title('Зависимость погрешности от заданной точности');
grid minor

clc, clear, close all
maxN = 5000;
E = [];
I = [];
for i = 1:15
    G=numgrid('H', 25);
    A=delsq(G); 
    P =ilup(A,2);
    n=size(A, 2);
    x0=ones(n, 1);
    xt=zeros(n,1);
    b=A*x0;
    e = 10^(-i);
    E = [E e];
    [x k, m] =conjgrad(A, xt, P, b,  e, maxN);
    I = [I k]
end

semilogx(E, I)
xlabel('Точность');
ylabel('Число итераций');
title('Зависимость числа итераций от заданной точности');
grid minor


clc, clear, close all
maxN = 5000;
e = 10^(-15);
E=[];
G=numgrid('H', 25);
A=delsq(G); 
P =ilup(A,2);
n=size(A, 2);
x0=ones(n, 1);
xt=zeros(n,1);
b=A*x0;
[x k, E] =conjgrad(A, xt, P, b,  e, maxN);
Iter = 1:k;
semilogy(Iter, E)
grid on
xlabel('Номер итерации')
ylabel('Погрешность')
title('Уменьшение погрешности с ходом итераций')


clc, clear, close all
maxN = 5000;
e = 10^(-15);
E=[];
K=[];
G=numgrid('H', 25);
A=delsq(G); 
P =ilup(A,2);
n=size(A, 2);
x0=ones(n, 1);
xt=zeros(n,1);
b=A*x0;
vozm=zeros(size(b, 1), 3);
for k=1:size(vozm, 2)
    for i=1:size(vozm, 1)
        vozm(i,k)=b(i)*(1+(-k+2*k*rand)/100);
    end
    K=[K k];
end
for j=1:size(vozm, 2)
    [x k m]=conjgrad(A, xt, P, vozm(:, j),  e, maxN);
    err=norm(x-x0)/norm(x0)*100
    E = [E err];
end 
plot(K, E)
grid on
ylabel('Относительная погрешность')
xlabel('Возмущение')
title('Зависимость относительной погрешности от возмущения')
