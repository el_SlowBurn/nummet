function x = pcd(A, b)
n = size(A,1);
y = zeros(n,1);
y(1) = b(1);

for i=2:n
    y(i)=b(i);
    for j = 1:i-1
        s = A(i,j)*y(j);
        y(i)=y(i)-s;
    end
end

x = zeros(n,1);
x(n)=y(n)/A(n,n);

for i = n-1:(-1):1
    x(i)=y(i);
    for j = (i+1):n
        x(i)=(x(i)-A(i,j)*x(j));
    end
    x(i)=x(i)/A(i,i);
end
end






        