clc, clear all;
XX1 = [];
XX2 = [];
N = [];
for s = 10 : 2000
    n = s;
    xt = ones(n, 1);
    A = eye(n, n);
    for i = 1:n
        A(i, n) = 1;
    end
   
    for i = 2:n
        for j = 1 : n-1
            if j < i
                A(i, j) = -1;
            end
        end
    end
    B = A * xt;
    x1 = A \ B;
    xx1 = norm( xt - x1) / norm( xt );

    [Q, R] = qr(A);
    y = Q' * B;
    x2 = R \ y;
    xx2 = norm( xt - x2) / norm( xt );
    
    XX1 = [XX1 xx1];
    XX2 = [XX2 xx2];
    N = [N n];
end
semilogy(N, XX1)
hold on
grid on
title('Зависимость ошибки от размера матрицы для LU')
xlabel('Ошибка')
ylabel('Размер')

figure
semilogy(N, XX2)
grid on
title('Зависимость ошибки от размера матрицы для QR')
xlabel('Ошибка')
ylabel('Размер')