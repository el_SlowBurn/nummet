function [y, Y] = Heun(f, y, h, a, b)
	x = a;
    Y = [y];
    
	while x < b - 0.0000000001
        y1 = y + h * f(x, y);
        y = y + (h / 2) * (f(x, y) + f(x + h, y1));
        x = x + h;
        Y = [Y y];
    end
end
