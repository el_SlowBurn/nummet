clc, close all, clear all;
format long 

a = 1;
b = 2;
h = 0.2;
y1 = @(x, y) 2 * x * (x.^2 + y);
y0 = exp(1);
Y = [];
Y2 = [];
X = [];
X2 = [];
X3=[];
Step = [];
EPS = [];
Err = [];
yy1 = 1;
yy2 = 2;
eps = 10.^(-7);
i = 0;

[yy1, Y1] = Heun(y1, y0, h, a, b);
[yy2, Y2] = Heun(y1, y0, h / 2, a, b);

for i = a:h:b
    X = [X i];
end
for i = a:h/2:b
    X2 = [X2 i];
end
for i = a:h/200:b
    X3 = [X3 i];
end
ext = @(x) x.* 2 .* exp(-x);
Y3 = ext(X3);

figure
plot(X, Y1);
hold on
plot(X2, Y2);
hold on
plot(X3, Y3, '--r')
title('Графики точного и численных решений')
legend("Решение, полученное с шагом h", "Решение, полученное с шагом h/2","Точное решение")

xx = [];
step = 0.001
for i = a:step:b
    xx = [xx i];
end
[~, Y4] = Heun(y1, y0, step, a, b);
YY4 = ext(xx);

figure
plot(xx, abs(Y4 - YY4))
grid on
title("График ошибки на отрезке")
xlabel("Координата")
ylabel("Ошибка")




%7
ERR = []
EPS = []
for i = 1:10
    eps = 10^(-i);
    EPS = [EPS eps];
    it = 0.2;
    xn = a:it:b;
    [~, Y5] = Heun(y1, y0, it, a, b);
    YY5 = ext(xn);
    while abs(Y5 - YY5) > eps
        it = it / 2;
        xn = a:it:b;
        [~, Y5] = Heun(y1, y0, it, a, b);
        YY5 = ext(xn);
    end
    ERR = [ERR abs(Y5 - YY5)];
end

figure
loglog(EPS, ERR, [1e-1, 1e-10], [1e-1, 1e-10], '--r')
title("График зависимости фактической ошибки от заданной точности")
xlabel("Точность")
ylabel("Ошибка")
