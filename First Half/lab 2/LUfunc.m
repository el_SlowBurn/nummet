function [L U] = LUfunc(A)
    L = zeros(size(A));
    U = zeros(size(A));
    n = size(A, 1);
    for i = 1:n
        L(i, i) = 1;
    end
    for i = 1:n
        for j = 1:i - 1
            L(i, j) = A(i, j);
            for k = 1:j - 1
                L(i, j) = (L(i, j) - L(i, k).* U(k, j));
            end
            L(i, j) = L(i, j)./ U(j, j);
        end
        for j = i:n
            U(i, j) = A(i, j);
            for k = 1:i - 1
                U(i, j) = U(i, j) - L(i, k).* U(k, j);
            end
        end
    end
end