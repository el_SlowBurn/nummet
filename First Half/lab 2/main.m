n = 10;
tol = 10 ^ (-13);
rep = 1000;
SEP = [];
ERR1 = [];
ERRV1 = [];
for k = 19:-0.5:1
    D = linspace(20, k, n);
    D = diag(D);
    x0 = ones(size(D, 1), 1);
    A = rand(n);
    [Q R] = qr(A);
    A = Q' * D * Q;
    [V1 d] = eig(A);
    d = diag(d);
    [d, in] = sort(d);
    V = V1(:, in);
    sep = d(2) - d(1);
    SEP = [SEP sep];
    [l count] = cycjacobi(A, tol);
    l = diag(l);
    l = sort(l);
    err = norm(d - l, inf);
    ERR1 = [ERR1 err];
    X = eigvector(A, l);
    Err = [];
    for k = 1:n
        Err = [Err abs(sin(acos(X(:, k)' * V(:, k)) / (norm(X(:, k)) * norm(V(:, k)))))];
    end
    errv = max(Err);
    ERRV1 = [ERRV1 errv];
end
plot(SEP, ERR1);
hold on
grid on
xlabel('Отделимость');
ylabel('Погрешность определния СЧ');
title('График зависимости погрешности определения СЧ от отделимости');


figure
plot(SEP, ERRV1);
hold on
grid on
xlabel('Отделимость');
ylabel('Погрешность определения СВ');
title('График зависимости погрешности определения СВ от отделимости');


n = 30;
rep = 1000;
TOL = [];
ERR1 = [];
ERRV1 = [];
COUNT1 = [];
k = 1;
D = diag(0.1:0.1:0.1*n);
x0 = ones(size(D, 1), 1);
A = rand(n);
[Q R] = qr(A);
A = Q' * D * Q;
[V1 d] = eig(A);
d = diag(d);
[d, in] = sort(d);
V = V1(:, in);
while k > (10 ^ (-15))
    [l count] = cycjacobi(A, k);
    l = diag(l);
    l = sort(l);
    err = norm(d - l, inf);
    ERR1 = [ERR1 err];
    X = eigvector(A, l);
    Err = [];
    for j = 1:n
        Err = [Err abs(sin(acos(X(:, j)' * V(:, j)) / (norm(X(:, j)) * norm(V(:, j)))))];
    end
    errv = max(Err);
    ERRV1 = [ERRV1 errv]; 
    COUNT1 = [COUNT1 count];
    TOL = [TOL k];
    k = k / 10;
end
figure
loglog(TOL, ERR1);
hold on
grid on
xlabel('Заданная точность')
ylabel('Погрешность')
title('Зависимость погрешности СЧ от заданной точности(плохая отделимость)')

figure
loglog(TOL, ERRV1);
hold on
grid on
xlabel('Заданная точность')
ylabel('Погрешность')
title('Зависимость погрешности СВ от заданной точности(плохая отделимость)')


figure
semilogx(TOL, COUNT1)
grid on
xlabel('Заданная точность')
ylabel('Число итераций')
title('Зависимость числа итераций от заданной точности(плохая отделимость)')
