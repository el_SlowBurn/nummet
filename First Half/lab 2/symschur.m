function [c, s] = symschur(A,p,q)
    if (A(p, q) == 0)
        c = 1;
        s = 0;
    else
        eta = (A(q, q) - A(p, p)) / (2 * A(p,q));
        if eta >= 0
            t = 1 / (eta + sqrt(1 + eta ^ 2));
        else
            t = -1 / (-eta + sqrt(1 + eta ^ 2));
        end
        c = 1 / sqrt(1 + t ^ 2);
        s = c * t;
    end
end
