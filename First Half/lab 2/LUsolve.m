function x = LUsolve(L, U, b)
    n = size(L, 1);
    y = zeros(size(b));
    y(1) = b(1);
    for i = 2:n
        y(i) = b(i);
        for j = 1:i - 1
            y(i) = y(i) - L(i, j) * y(j);
        end
    end
    x = zeros(size(b));
    x(n) = y(n) / U(n, n);
    for i = n - 1:(-1):1
        x(i) = y(i);
        for j = (i + 1):n
            x(i) = (x(i) - U(i, j) * x(j));
        end
        x(i) = x(i) / U(i, i);
    end
end