function [D, sweep] = cycjacobi(A,toll)
    n = max(size(A));
    D = A;
    psiD = norm(A,'fro');
    epsi = toll * psiD;    
    n = max(size(D));
    psi = 0;
    for i=1:(n-1)
        for j=(i+1):n
            psi = psi + D(i,j)^2+D(j,i)^2;
        end
    end
    psiD = sqrt(psi);
    
    sweep = 0;
    while (psiD >= epsi)
        sweep = sweep + 1;
        for p = 1:(n - 1)
            for q = (p + 1):n
                [c,s] = symschur(D, p, q);
                [D] = gacol(D, c, s, 1, n, p, q, 1);
                [D] = gacol(D, c, s, 1, n, p, q, 2);
            end
        end
        n = max(size(D));
        psi = 0;
        for i=1:(n-1)
            for j=(i+1):n
                psi = psi + D(i,j)^2+D(j,i)^2;
            end
        end
        psiD = sqrt(psi);
    end
end
