clc, clear
f=@(x) (x.^4-2.2*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^4);
q=integral(f, -2, 2);
E=[];
I0 = 0;
TOL=[];
COUNT=[];
i=1;
while i>(10^(-9))
    [I, k, ~]=ad_left_rect(f, -2, 2, i, I0);
    e=abs(I-q);
    E=[E e];
    COUNT=[COUNT k];
    TOL=[TOL i];
    i=i*0.1
end
x = -30:-1;
y = @(x) x;
plot(log2(TOL), log2(E));
hold on
plot(x, y(x))
grid on
xlabel('Заданная точность')
ylabel('Фактическая ошибка')
title('Зависимость фактической ошибки от заданной точности')
legend('Рекурсия', 'Биссектриса')

figure
plot(log2(TOL), log2(COUNT))
hold on
grid on
xlabel('Заданная точность')
ylabel('Число итераций')
title('Зависимость числа итерация от заданной точности')
legend('Рекурсия')

tol=1e-1;
[~, ~, X]=ad_left_rect(f, -2, 2, tol, I0);
X=sort(X)
S=zeros(size(X));
for i=1:size(X, 2)-1
    S(i)=X(i+1)-X(i);
end
figure
semilogy(X, S)
hold on
grid on
xlabel('Координата')
ylabel('Длина отрезка разбиениея')
title('Зависимость длины отрезка разбиения от координаты')

%несобственные интегралы
f1 = @(x) x./(x.^4+1);
q = pi/4;

%f2 = @(x) 1./(nthroot(3-4.*x,5))
%q = -5/16;

E=[];
TOL=[];
COUNT=[];
i=1;
B1=1e3;
B2=1e-8;
I01=(B1).*f1((B1));
%I02=(1-(3/4+B2)).*f2((1-(3/4+B2))/2);
while i>(10^(-5))
    [I, k, ~]=ad_left_rect(f1, 0, B1, i, I01);
    %[I, k, ~]=asimpson(f2, 3/4+B2, 1, i, I02);
    e=abs(I-q);
    E=[E e];
    COUNT=[COUNT k];
    TOL=[TOL i];
    i=i*0.1
end
x = -17:-1;
y = @(x) x;
figure
plot(log2(TOL), log2(E));
hold on
plot(x, y(x))
grid on
xlabel('Заданная точность')
ylabel('Фактическая ошибка')
title('Зависимость фактической ошибки от заданной точности (рекурсия)')
legend('II рода', 'Биссектриса')

figure
plot(log2(TOL), log2(COUNT))
hold on
grid on
xlabel('Заданная точность')
ylabel('Число итераций')
title('Зависимость числа итераций от заданной точности (II рода)')

tol=1e-5;
[~,~, X]=ad_left_rect(f1, 0, B1, i, I01);
[~,~, X]=ad_left_rect(f2, 3/4+B2, 1, i, I02);
X=sort(X);
S=zeros(size(X));
for i=1:size(X, 2)-1
    S(i)=X(i+1)-X(i);
end
figure
semilogy(X, S)
hold on
grid on
xlabel('Координата')
ylabel('Длина отрезка разбиения')
title('Зависимость длины отрезка разбиения от координаты (I рода)')

% c1=(2.2*(1+(-1+2*rand)/100))
% f1=@(x) (x.^4-c1.*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^4);
% c2=(5.2*(1+(-2+2*2*rand)/100))
% f2=@(x) (x.^4-c2.*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^4);
% c3=(5.2*(1+(-3+2*3*rand)/100))
% f3=@(x) (x.^4-c3.*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^4);
% q=integral(f, -2, 2);

E1=[];
E2=[];
E3=[];
TOL=[];
COUNT1=[];
COUNT2=[];
COUNT3=[];
i=1;
while i>(10^(-8))
    c1=(2.2*(1+(-1+2*rand)/100))
    f1=@(x) (x.^4-c1.*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^4);
    c2=(5.2*(1+(-2+2*2*rand)/100))
    f2=@(x) (x.^4-c2.*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^4);
    c3=(5.2*(1+(-3+2*3*rand)/100))
    f3=@(x) (x.^4-c3.*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^4);
    q=integral(f, -2, 2);
    [I, k, ~]=ad_left_rect(f1, -2, 2, i,I0);
    e=abs(I-q);
    E1=[E1 e];
    COUNT1=[COUNT1 k];
    [I, k, ~]= ad_left_rect(f2, -2, 2, i,I0);
    e=abs(I-q);
    E2=[E2 e];
    COUNT2=[COUNT2 k];
    [I, k, ~]= ad_left_rect(f2, -2, 2, i,I0);
    e=abs(I-q);
    E3=[E3 e];
    COUNT3=[COUNT3 k];
    TOL=[TOL i];
    i=i*0.1
end
x = -30:-1;
y = @(x) x;
figure
plot(log2(TOL), log2(E1));
hold on
plot(log2(TOL), log2(E2));
plot(log2(TOL), log2(E3));
plot(x, y(x))
grid on
xlabel('Заданная точность')
ylabel('Фактическая ошибка')
title('Зависимость фактической ошибки от заданной точности')
legend('1%','2%', '3%', 'Биссектриса')
figure
plot(log2(TOL), log2(COUNT1))
hold on
plot(log2(TOL), log2(COUNT2))
plot(log2(TOL), log2(COUNT3))
grid on
xlabel('Заданная точность')
ylabel('Число итераций')
title('Зависимость числа итераций от заданной точности')
legend('1%','2%', '3%')
