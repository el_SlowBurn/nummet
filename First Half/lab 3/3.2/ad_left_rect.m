function [I,k,H] = ad_left_rect(f, a, b, eps, I0)
    c = (a + b) / 2;
    k = 1;
    I1 = left_rect(f, a, c, eps);
    I2 = left_rect(f, c, b, eps);
    I = I1+I2;
    e = abs(I - I0)/15;
    H = c;
    if e > eps
        [Ia, ka ,ha] = ad_left_rect(f, a, c, eps/2,I1);
        [Ib, kb, hb] = ad_left_rect(f, c, b, eps/2,I2);
         I = Ia + Ib;
        k = ka + kb+k;
        H = [ha hb];
    else
        return
    end
end
