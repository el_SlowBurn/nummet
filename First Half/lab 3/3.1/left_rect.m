function [V, iter] = left_rect(f,a,b,eps, max_itter)
    n = 10; 
    h = (b - a) / n;
    x = a:h:b;
    y_prev = 0;
    y = 0;
    for i = 1:n
        y = y + f(x(i));
    end
    iter = 1;
    while iter <= max_itter
        y_prev = y;
        n = 2 * n;
        h = (b - a) / n;
        x = (a + h):(2 * h):(b - h);
        for i = 1:(n / 2)
            y = y + f(x(i));
        end
        iter = iter + 1;
        if abs((h * y) - (2 * h * y_prev)) <= eps
            V = h * y;
            break
        end
    end
end
