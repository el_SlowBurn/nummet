clc
clear all
close all
eps = 1e-7;
Q = -(109/30);
f = @(x) x.^5 -3.2*x.^3 + 1.5*x.^2 - 7*x;
[q,i, step, pV] = left_rect_old(f,0,1,eps);
Delta = abs(pV-Q);

figure 
Ls = log2(step);
LD = log2(Delta);
plot(Ls, LD)
grid on
title({'График зависимости','фактической погрешности от шага сетки'})
xlabel('h', FontSize=18)
ylabel('\delta', FontSize=18)

p = polyfit(Ls,LD,1);
n = p(1)
C = 2^(p(2))
