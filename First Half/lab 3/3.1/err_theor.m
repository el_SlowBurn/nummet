clc
clear all
close all
max_itter = 100000;
Q = -(109/30);
f = @(x) x.^5 -3.2*x.^3 + 1.5*x.^2 - 7*x;
E = [];
Delta = [];
N = [];
TheorErrorMax = [];
TheorErrorMin = [];
a = 0;
b = 1;
M1 = 8.6;
for i = 1:7
    i
    eps = 10^(-i);
    E = [E eps];
    [q,i] = left_rect(f, a, b, eps, max_itter);
    Delta = [Delta abs(q - Q)];
    N = [N i];
    h = (b-a)/i;
    Rmax = abs((b-a)^2/2*h*M1);
    TheorErrorMax = [TheorErrorMax Rmax];
    TheorErrorMin = [TheorErrorMin -Rmax];
end

figure
y = @(x) x;
x = 1e-7:0.1:1;
plot(x,y(x))
hold on
plot(E,Delta)
plot(E,TheorErrorMax)
plot(E,TheorErrorMin)
grid on
title('График зависимости погрешности от точности')
xlabel('\epsilon', FontSize=18)
ylabel('\delta', FontSize=18)
legend('y=x', 'Фактическая погрешность', 'Максимальная теоретическая погрешность', 'Минимальная теоретическая погрешность')

figure
semilogx(E, N)
grid on
title('График зависимости кол-ва итераций от точности')
xlabel('\epsilon', FontSize=18)
ylabel('N', FontSize=18)
