clc
clear all
close all
max_itter = 100000;
Q = -(109/30);
f = @(x) x.^5 -3.2*x.^3 + 1.5*x.^2 - 7*x;
E = [];
Delta = [];
N = [];
for i = 1:7
    i
  eps = 10^(-i);
  E = [E eps];
[q,i] = left_rect(f,0,1,eps, max_itter);
Delta = [Delta abs(q-Q)];
N = [N i];
end

figure
y = @(x) x;
x = 1e-7:0.1:1;
loglog(x,y(x))
hold on
loglog(E,Delta)
grid on
title('График зависимости погрешности от точности')
xlabel('\epsilon', FontSize=18)
ylabel('\delta', FontSize=18)
legend('y = x')

figure
semilogx(E, N)
grid on
title('График зависимости кол-ва итераций от точности')
xlabel('\epsilon', FontSize=18)
ylabel('N', FontSize=18)
