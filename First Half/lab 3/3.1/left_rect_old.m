function [V, iter, step, pV] = left_rect_old(f,a,b,eps)


    n = 10; 
    step = [];
    pV = []; 

    for j = 1:100000 
        
        h = (b-a)/n; 
        step = [step h];
        
        xi = a:h:b; 
        y = 0; 
        
        for i = 1:n-1 
            y = y + f(xi(i));
        end
        
        I1 = h*y; 
        pV = [pV I1]; 
        
        if j == 1 
            I = 0;
        end
        
        if abs(I-I1) < eps 
            V = I1; 
            iter = j;
            break
        end
        
        n = 2*n;
        I = I1; 
        
    end

end
