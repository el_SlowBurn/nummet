f = @(x) x.^5 -3.2*x.^3 + 1.5*x.^2 - 7*x; 
a = 0; 
b = 1; 


n0 = 10; 
eps = 1e-5; 


h = (b-a)/n0; 
I0 = h*sum(f(a + h*(0:n0-1))); 


n1 = 2*n0; 
h = (b-a)/n1;
I1 = h*sum(f(a + h*(0:n1-1))); 


p = 1; 
delta = (I1 - I0)/(2^p - 1); 


while abs(delta) > eps 
    n0 = n1;
    I0 = I1;
    n1 = 2*n0;
    h = (b-a)/n1;
    I1 = h*sum(f(a + h*(0:n1-1)));
    delta = (I1 - I0)/(2^p - 1);
end

fprintf('Приближенное значение интеграла: %.6f\n', I1);
fprintf('Число разбиений отрезка: %d\n', n1);
fprintf('Оценка погрешности: %.6f\n', delta);
