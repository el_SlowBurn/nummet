f=@(x) x.^4-2.2*x.^3+0.5*x.^2-7*x-3.4;
q=186/5;
e=0;
TOL=[];
E=[];
for p=2:10
    I=integral(f, -2, 2, 'RelTol', 10^(-p));
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');

f=@(x) sign(x).*(x.^4-2.2*x.^3+7.5*x.^2-7*x-3.9);
TOL=[];
E=[];
q=-45.6;
e=0;
for p=2:10
    I=integral(f, -2, 2, 'RelTol', 10^(-p));
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');

%№2
f=@(x) (x.^4-2.2*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^2);
q=8.7399;
TOL=[];
E=[];
e=0;
for p=2:10
    I=integral(f, -2, 2, 'RelTol', 10^(-p));
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');

%№3
f=@(x) (x.^4-2.2*x.^3+7.5*x.^2-7*x-3.9).*sin(x.^2);
q=8.7399;
TOL=[];
E=[];
e=0;
for p=2:10
    I=quadgk(f,-2, 2, 'RelTol', 10^(-p));
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');

%№4
f2= @(x, y) y.^3-2.2*x.^3+7.5*x.^2-7*x-3.9;
q=488/5;
TOL=[];
E=[];
e=0;
for p=2:10
    I=integral2(f2,-2, 2, -2, 2, 'RelTol', 10^(-p)) ;
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');

%№5
f3= @(x, y) (y.^3-2.2*x.^3+7.5*x.^2-7*x-3.9).*(x.^2 + y.^2 <= 1)
q=integral2(f3,-2, 2, -2, 2,'RelTol', 0);
TOL=[];
E=[];
e=0;
for p=2:10
    I=integral2(f3,-2, 2, -2, 2, 'RelTol', 10^(-p)) ;
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');

%№6
f4=@(x, y, z) y.^3-2.2*x.^3+7.5*x.^2-7*z.^2-3.9;
q=-3104/15;
TOL=[];
E=[];
e=0;
for p=2:10
    I=integral3(f4,-2, 2, -2, 2, -2, 2, 'RelTol', 10^(-p));
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');

%№7.3
f=@(x) 1./(nthroot(3-4.*x,5));
q=-5/16;
TOL=[];
E=[];
e = 0;
for p=1:7
    I=variation(f, 3/4, 1, 3/4, 10^(-p))
    e=abs(I-q);
    E=[E e];
    TOL = [TOL p];
end
semilogy(TOL, E)
grid on
xlabel('Степень точности');
ylabel('Погрешность');
title('Зависимость погрешности от степени точности');
