function I=variation(f, a, b, x0, tol)
    left=0.1;
    right=0.1;
    if x0==b
        right=0;
    elseif x0==a;
        left=0;
    end
    int=integral(f, x0-left, x0+right);
    while(abs(int)>tol)
        right=right*0.1;
        left=left*0.1;
        int=integral(f, x0-left, x0+right);
    end
    if x0==b
        I=integral(f, a, x0-left);        
    elseif x0==a
        I=integral(f, x0+right, b);
    else
        I=integral(f, a, x0-left)+integral(f, x0+right, b);
    end
end