function [res] = gass(f, k, a, b)
    s = 0;
    [xx, ww] = uzel(k);
    h = ww(1) - ww(2);
    for i = 1:k
        s = s + f(((a + b) / 2) + (b - a) * ww(i) / 2) * xx(i);
    end
    res = (b - a) * s / 2;
end
