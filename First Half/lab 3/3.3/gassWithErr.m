function [res] = gassWithErr(f, k, a, b, er)
    s = 0;
    [xx, ww] = uzel(k);
    ww(2) = ww(2) * er;
    for i = 1:k
        s = s + f(((a + b) / 2) + (b - a) * ww(i) / 2) * xx(i);
    end
    res = (b - a) * s / 2;
end