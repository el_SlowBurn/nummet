function [I, n] = rec_gass(f, k, a, b, eps, I0, i)
    c = (a + b) / 2;
    I1 = gass (f, k, a, c);
    I2 = gass (f, k, c, b);
    I = I1 + I2;
    e = abs(I - I0);
    n = i;
    if e > eps
        nc = n;
        n = n + 1;
        [Ia, na] = rec_gass(f, k, a, c, eps/2, I1, n);
        nc = nc + 1;
        [Ib, nb] = rec_gass(f, k, c, b, eps/2, I2, nc);
        I = Ia + Ib;
        n = na + nb;
    else
        return
    end
end