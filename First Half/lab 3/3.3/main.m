clc, close all, clear all;

f = @(x) (x.^4 - 2.2.^x.^3 + 0.5.*x.^2 - 7.*x - 3.4).*cos(2.*x);

a = 2;
b = 3;
realInt = quadgk(f, a, b);
tol = 1;
Iter = [];
Tol = [];
Err = [];
H = [];
eps = 10 ^ (-5);

for i = 2:4
    I = gass(f, i, a, b);
    % Iter = [Iter I];
    Tol = [Tol abs(I - realInt)];
    H = [H i];
end

I1 = [];
I2 = [];
I3 = [];
toc = [];
it1 = [];
it2 = [];
it3 = [];
for i = 1:10
    [av, n] = rec_gass(f, 2, a, b, 10^(-i), 0, i);
    I1 = [I1 abs(av - realInt)];
    it1 = [it1 n];
    [av2, n] = rec_gass(f, 3, a, b, 10^(-i), 0, i);
    I2 = [I2 abs(av2 - realInt)];
    it2 = [it2 n];
    [av3, n] = rec_gass(f, 4, a, b, 10^(-i), 0, i);
    I3 = [I3 abs(av3 - realInt)];
    it3 = [it3 n];
    toc = [toc 10^(-i)];
end
plot(H, Tol)
title('Зависимость ошибки от степени полинома')
ylabel('Ошибка')
xlabel('Степень полинома')

figure
loglog(toc, I1, toc, I2, toc, I3, [1e-1, 1e-10], [1e-1, 1e-10])
legend('n = 2', 'n = 3', 'n = 4')
title('Зависимость фактической ошибки от точности')
xlabel('Точность')
ylabel('Ошибка')

figure
plot(toc, I1, toc, I2, toc, I3)
legend('n = 2', 'n = 3', 'n = 4')
title('Зависимость фактической ошибки от точности')
xlabel('Точность')
ylabel('Ошибка')

figure
plot(toc, it1, toc, it2, toc, it3)
legend('n = 2', 'n = 3', 'n = 4')
title('Зависимость количества итераций от точности')
xlabel('Ошибка')
ylabel('Итерации')



IEr1 = [];
for j = 2:4
    tr = gassWithErr(f, j, a, b, (1.01));
    IEr1 = [IEr1 abs(tr - realInt)]; 
end
IEr2 = [];
for j = 2:4
    tr = gassWithErr(f, j, a, b, (1.02));
    IEr2 = [IEr2 abs(tr - realInt)]; 
end
IEr3 = [];
for j = 2:4
    tr = gassWithErr(f, j, a, b, (1.03));
    IEr3 = [IEr3 abs(tr - realInt)]; 
end
figure
plot(H, Tol,'--r', H, IEr1, H, IEr2, H, IEr3)
legend('0%', '1%', '2%', '3%')
title('Зависимость ошибки от степени полинома с погрешностью')
ylabel('Ошибка')
xlabel('Степень полинома')