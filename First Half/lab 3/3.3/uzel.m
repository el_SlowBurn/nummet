function [xx, ww] = uzel(k)
    format long
    syms z;
    s = (z ^ 2 - 1) ^ k; 
    S = diff(s, k); 
    P = sym2poly(S);
    t = roots(P);
    if k == 2
        G = (z - t(2));
        g = G / (t(1) - t(2));
        A1 = double(int(g, -1, 1));
        G = (z - t(1));
        g = G / (t(2) - t(1));
        A2 = double(int(g, -1, 1));
        xx = [A1, A2];
    end
    if k == 3
        G = (z - t(2)) * (z - t(3));
        g = G / ((t(1) - t(2)) * (t(1) - t(3)));
        A1 = double(int(g, -1, 1));
        G = (z - t(1)) * (z - t(3));
        g = G / ((t(2) - t(1)) * (t(2) - t(3)));
        A2 = double(int(g, -1, 1));
        G = (z - t(1)) * (z - t(2));
        g = G / ((t(3) - t(1)) * (t(3) - t(2)));
        A3 = double(int(g, -1, 1));
        xx = [A1, A2, A3];
    end
    if k == 4
        G = (z - t(2)) * (z - t(3)) * (z - t(4));
        g = G / ((t(1) - t(2)) * (t(1) - t(3)) * (t(1) - t(4)));
        A1 = double(int(g, -1, 1));
        G = (z - t(1)) * (z - t(3)) * (z - t(4));
        g = G / ((t(2) - t(1)) * (t(2) - t(3)) * (t(2) - t(4)));
        A2 = double(int(g, -1, 1));
        G = (z - t(1)) * (z - t(2)) * (z - t(4));
        g = G / ((t(3) - t(1)) * (t(3) - t(2)) * (t(3) - t(4)));
        A3 = double(int(g, -1, 1));
        G = (z - t(1)) * (z - t(2)) * (z - t(3));
        g = G / ((t(4) - t(2)) * (t(4) - t(3)) * (t(4) - t(1)));
        A4 = double(int(g, -1, 1));
        xx = [A1, A2, A3, A4];
    end
    ww = t;
end