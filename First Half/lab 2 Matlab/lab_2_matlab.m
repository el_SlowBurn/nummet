clc, clear

% A.1
n = 100;
d = diag(1:n);
d1 = rand(n);
[Q, R] = qr(d1);
A1 = Q' * d * Q;
figure 
spy(A1)
[V,D] = eig(A1);
zero = norm(A1*V-V*D)

% A.2
n = 500;
d = diag(1:n);
d1 = rand(n);
[Q, R] = qr(d1);
A2 = Q' * d * Q;
figure
spy(A2)
[V,D] = eig(A2);
zero = norm(A2*V-V*D)

% B.1
n = 500;
d = diag(1:n);
d1 = rand(n);
[Q, R] = qr(d1);
B1 = Q' * d * Q;
for i = 1:n
    for k = 1:n
        if B1(i,k) == 0
            B1(i,k) = 1e-16;
        end
    end
end
figure 
spy(B1)
[V,D] = eig(B1);
zero = norm(B1*V-V*D)

% B.2
n = 500;
d = diag(1:n);
d1 = rand(n);
[Q, R] = qr(d1);
B2 = Q' * d * Q;
for i = 1:n
    for k = 1:n
        if B2(i,k) == 0
            B2(i,k) = 1e-100;
        end
    end
end
figure 
spy(B2)
[V,D] = eig(B2);
zero = norm(B2*V-V*D)


d = sort(diag(d));
tic
a = eig(B2);
toc
a = sort(a);
e = norm(a-d) / norm(d)

% Step 2
B = rand(size(B1, 1));
tic
a = eig(B1, B);
toc
P = inv(B) * B1;
d = eig(P);
d = sort(d);
a = sort(a);
e = norm(a - d) / norm(d)

% Step 3
d = sort(diag(d));
tic
[V, D] = eig(B2);
toc
D=sort(diag(D));
e = norm(D-d) / norm(d)

% Step 4
d = sort(diag(d));
tic
[V, D]=eig(B2, 'nobalance');
toc
D=sort(diag(D));
e = norm(D - d) / norm(d)

% Step 5
d = sort(diag(d));
B = rand(size(B2, 1));
tic
[V, D]=eig(B2, B);
toc
P = inv(B) * B2;
D = sort(diag(D));
a = eig(P);
a = sort(a);
e = norm(D - a) / norm(a)

% Step 6.1
d = sort(diag(d));
B = rand(size(A2, 1));
tic
[V, D] = eig(A2, B, 'chol');
toc
P = inv(B) * A2;
D = sort(diag(D));
a = eig(P);
a = sort(a);
e = norm(D - a) / norm(a)

% Step 6.2
d = sort(diag(d));
B = rand(size(B2, 1));
tic
[V, D] = eig(B2, B, 'qz');
toc
P = inv(B) * B2;
D = sort(diag(D));
a = eig(P);
a = sort(a);
e = norm(D - a) / norm(a)

%QR
d = sort(diag(d));
k=3000; 
tic
B2=hess(B2);
for i=1:k
    [Q,R]=qr(B2);
    B2=R*Q;
end
toc
D=diag(B2);
D=sort(D);
e = norm(D-d)/norm(d)