clc, clear
len = 10;
X = ones(len, 1); %вектор-столбец с 10-ю единицами
X0 = rand(len, 1);
eps = 10 ^ (-3);
n = 30;
A = rand(len);
A = A + A' + ones(len) * 10;
B = A * X;
for i = 1:15
    opac(i) = 10 ^ (-i);
    [r(:, i), iter(i)] = MSG(A, B, X0, opac(i), 1000);
end
plot (iter, -log10(opac))
ylabel('Прологорифмированная точность')
xlabel('Итеррации')