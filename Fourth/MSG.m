function [solution, iter] = MSG(a, b, x0, eps, n)    
    r(:, 1) = b - a * x0;
    z(:, 1) = r(:, 1);
    x(:, 1) = x0
    alpha(1) = 1;
    betta(1) = 1;
    if n == 1
        n = 2;
    end
    for i = 2:n
        alpha(i) = dot(r(:, i - 1), r(:, i - 1)) / dot((a * z(:, i - 1)), z(:, i - 1))
        x(:, i) = x(:, i - 1) + alpha(i) * z(:, i - 1)
        r(:, i) = r(:, i - 1) - alpha(i) * a * z(:, i - 1)
        betta(i) = dot(r(:, i), r(:, i)) / dot(r(:, i - 1), r(:, i - 1))
        z(:, i) = r(:, i) + betta(i) * z(:, i-1)
        
        if abs(max(abs(x(:, i) - x(:, i - 1)))) <= eps
            iter = i;   
            solution = x(:, i);
            break
        end
    end
    iter = i;
    solution = x(:, i);
end