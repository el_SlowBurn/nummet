clc, close all, clear all
format long
%относительная погрешность = норма (х с возмущением - х точное)/норма(х точное) от возмущения
len = 10;
X = ones(len, 1); %вектор-столбец с 10-ю единицами
X0 = rand(len, 1)
eps = 10 ^ (-3);
n = 30;
A = rand(len);
A = A + A' + ones(len) * 10;
[u, d, v] = svd(A);
d = eye(len);
d(1,1) = 1; %задаем число обусловленности
M = u * d * v'; %свернули матрицу обратно
B = M * X; %умножили матрицу на вектор, получили правую часть

[L, U, P] = lu(M);
Y = L \ (P * B);
R0 = U \ Y; % исходное решение

for i = 1:3
    if rand < 0.5
        pn = 1;
    else
        pn = -1;
    end
    [aa, ind] = max(M);
    [bb, ind2] = max(aa);
    M(ind(ind2)) = M(ind(ind2)) * (1 + 0.01 * i * pn);
    B = M * X;
    [R(:, i), ITER(i)] = MSG(M, B, X0, eps, n);
    ERR(i) = norm(R(i) - R0) / norm(R0);
    M(ind(ind2)) = bb;
    r(i) = i;
end

plot(r, ERR, "o-")
grid on
% title("Зависимость относительной погрешности от возмущения максимального элемента матрицы")
xlabel("Процент возмущения")
ylabel("Относительная погрешность")


