
clc, close all, clear all
len = 10;
eps = 10 ^ (-3);
n = 30;
X0 = rand(len, 1);
X = ones(len, 1); %вектор-столбец с 10-ю единицами, точное решение

for i = 1:6
    A = rand(len);
    A = A + A' + ones(len) * 10;
    [u, d, v] = svd(A);
    d = eye(len);
    d(1,1) = 10 ^ (i - 1); %задаем число обусловленности
    M = u * d * v'; %свернули матрицу обратно
    c(i) = cond(M);
    B = M * X; %умножили матрицу на вектор, получили правую часть
    tic
    [R, ITER(i)] = MSG(M, B, X0, eps, n);

    t(i) = toc;
    [L0, U0, P] = lu(M);
    Y0 = L0 \ (P * B);
    R0 = U0 \ Y0;
    h(i) = norm(R0 - R);
end

figure
loglog(c, h, ".-")
% title("График зависимости ошибки от числа обусловленности")
xlabel("Число обусловленности")
ylabel("Прологарифмированная норма отклонения")
grid on

figure
semilogx(c, t)
% title("График зависимости времени от числа обусловленности")
xlabel("Число обусловленности")
ylabel("Прологарифмированное время")
grid on