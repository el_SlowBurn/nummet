clc, clear
len = 10;
X = ones(len, 1); %вектор-столбец с 10-ю единицами
X0 = rand(len, 1);
eps = 10 ^ (-3);
n = 30;
A = rand(len);
A = A + A' + ones(len) * 10;
B = A * X;

[L, U, P] = lu(A);
Y = L \ (P * B);
R0 = U \ Y;
for i = 1:20
    iteration(i) = i;
    [r(:, i), iter] = MSG(A, B, X0, 10^100, i);
    ERR(i) = norm(r(i) - R0)/norm(R0);
end
plot(iteration, ERR)
xlabel('Итерации')
ylabel('Погрешность')