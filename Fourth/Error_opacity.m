clc, clear
len = 10;
X = ones(len, 1); %вектор-столбец с 10-ю единицами
X0 = rand(len, 1);
eps = 10 ^ (-3);
n = 30;
A = rand(len);
A = A + A' + ones(len) * 10;
B = A * X;

[L, U, P] = lu(A);
Y = L \ (P * B);
R0 = U \ Y;

for i = 1:10
    opac(i) = 10 ^ (-i);
    [r(:, i), iter] = MSG(A, B, X0, opac(i), 1000);
    ERR(i) = norm(r(i) - R0)/norm(R0);
end
semilogx(opac, ERR)
xlabel('Прологорифмированная точность')
ylabel('Погрешность')